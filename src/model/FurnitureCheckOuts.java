package model;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;

public class FurnitureCheckOuts extends FurnitureTransaction {

	private int checkoutID;
	private Customer customer;
	private Employee overseeingEmployee;
	private Furniture furnitureItem;
	private Timestamp checkoutDate;
	private Date dueDate;
	private int daysTillDue;
	private int quantity;
	
	/**Stores and retrieves information related to a single furniture checkout item.
	 * 
	 * @Postcondition: as this(furnitureItem, checkout, dueDate, qty), plus this.checkoutID == checkoutID, this.customer == customer, and this.overseeingEmployee == employee
	 * @param checkoutID: The ID for this rental item.
	 * @param customer: The customer renting this item.
	 * @param overseeingEmployee: The employee checking out this customer.
	 * For the other params, see this(furnitureItem, checkout, dueDate, qty).
	 */
	public FurnitureCheckOuts(int checkoutID, Customer customer, Employee overseeingEmployee, Furniture furnitureItem, Timestamp checkOut,
			Date dueDate) {
		this(furnitureItem, checkOut, dueDate, 1);
		this.checkoutID = checkoutID;
		this.customer = customer;
		this.overseeingEmployee = overseeingEmployee;

	}
	
	/**Stores and retrieves information related to a single furniture checkout item.
	 * 
	 * @Postcondition this.furnitureItem == furnitureItem, this.checkoutDate == checkOut, this.dueDate == dueDate, this.quantity == qty
	 * @param furnitureItem: The item of furniture being rented.
	 * @param checkOut: The timestamp of the checkout time.
	 * @param dueDate: The date this rental item is due.
	 * @param qty: The amount of this furniture item being rented.
	 */
	public FurnitureCheckOuts(Furniture furnitureItem, Timestamp checkOut, Date dueDate, int qty) {
		super(furnitureItem, checkOut, qty);		
		this.furnitureItem = furnitureItem;
		this.setCheckoutDate(checkOut);
		this.setDueDate(dueDate);
		this.setQuantity(qty);
	}
	
	/**
	 * Gets the id of this checkout.
	 * @return the id of this checkout.
	 */
	public int getCheckoutID() {
		return checkoutID;
	}
	/**
	 * Sets the id of this checkout.
	 * @postcondition: this.checkoutID == checkoutID
	 * @param checkoutID the new checkout ID
	 */
	public void setCheckoutID(int checkoutID) {
		this.checkoutID = checkoutID;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Employee getOverseeingEmployee() {
		return overseeingEmployee;
	}
	public void setOverseeingEmployee(Employee overseeingEmployee) {
		this.overseeingEmployee = overseeingEmployee;
	}
	public Furniture getFurnitureItem() {
		return furnitureItem;
	}
	public void setFurnitureItem(Furniture furnitureItem) {
		this.furnitureItem = furnitureItem;
	}
	public Timestamp getCheckoutDate() {
		return checkoutDate;
	}
	public void setCheckoutDate(Timestamp checkoutDate) {
		this.checkoutDate = checkoutDate;
	}
	public Date getDueDate() {
		return this.dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public void setDaysTillDue(int days) {
		daysTillDue = days;
		setDueDate(Date.valueOf(LocalDate.now().plusDays(days)));
	}
	public int getDaysUntilDue() {
		return daysTillDue;
	}
	@Override
	public String toString() {
		return getFurnitureItem().getDescription();
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getDescription() {
		return furnitureItem.getDescription();
	}

	/**
	 * @return the fID
	 */
	public int getFid() {
		return furnitureItem.getFid();
	}

	/**
	 * @return the price
	 */
	public double getDailyRate() {
		return furnitureItem.getDailyRate();
	}
	
	public double getLateFee() {
		return furnitureItem.getLateFee();
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return furnitureItem.getStyle();
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return furnitureItem.getType();
	}
	
	public int getOnHand() throws SQLException {
		return getFurnitureItem().getOnHand();
	}
	
}
