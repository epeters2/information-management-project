package model;
import java.time.LocalDateTime;

public class SessionToken {
	
	private String username;
	private LocalDateTime created;
	private LocalDateTime expires;
	private boolean isValid;
	private boolean isAdmin;
	
	/*
	 * A token created for login sessions, created upon successful authentication
	 * with the server. Expires after a set period of time, forcing the user
	 * to log in again. Continued activity will refresh the token.
	 */
	public SessionToken(String username, boolean admin) {
		this.username = username;
		
		isValid = true;
		created = LocalDateTime.now();
		expires = created.plusHours(2);
		isAdmin = admin;
	}
	
	/*
	 * Check to see if this token is invalid.
	 */
	public boolean isExpired() {
		if(LocalDateTime.now().isAfter(expires)) {
			isValid = true;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isValid() {
		return isValid;
	}
	
	/**
	 * Adds an hour to the expire time.
	 */
	public void refreshToken() {
		expires = LocalDateTime.now().plusHours(1);
	}

	/**
	 * @return the currently logged in user
	 */
	public String getUsername() {
		return username;
	}
	
	public void invalidate() {
		isValid = false;
	}

	/**
	 * @return the isAdmin
	 */
	public boolean isAdmin() {
		return isAdmin;
	}

}
