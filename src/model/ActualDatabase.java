package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;
/**
 * Class for handling database connections.
 */
public class ActualDatabase extends DataSource {
	
	String connectionString = "jdbc:mysql://160.10.25.16:3306/cs3230f19a?user=cs3230f19a&password=77Fvd8Hotj6qPBqc&serverTimezone=EST&zeroDateTimeBehavior=convertToNull";

	Connection connection = null;
	
	/**
	 * Try to establish a connection to the db.
	 */
	public void establishConnection() {
		try {
			connection = DriverManager.getConnection(connectionString);
		} catch (CommunicationsException e) {
			System.err.println(e);			
		} catch (SQLException e) {
			System.err.println(e);
		}
	}
	
	/**
	 * Tries to execute a SQL query against the database, if connected.
	 * @param the query to be passed to the database.
	 * @return the ResultSet resulting from the query.
	 */
	@Override
	public String runSQLQuery(String query) {
		PreparedStatement stmt = null;
		
			
			try {
				this.establishConnection();
				stmt = connection.prepareStatement(query);
				stmt.execute();
			} catch (SQLException e) {
				// TODO Replace with handling and proper logging to file
				System.err.println(e);
				// TODO Replace this with a nice looking message to the user.
				System.err.println("Could not execute query. Not connected to the database.");

			} finally {
				this.closeConnection();
			}
		
		return "";
	}
	
	/**
	 * Disconnect from the db.
	 * @postcondtion: this.connection is closed, this.connection == null.
	 */
	public void closeConnection() {
		try {
			this.connection.close();
			this.connection = null;
		} catch (SQLException e) {
			// TODO Replace with proper handling and printing to log file
			System.err.println(e);
		}
		
	}
	
	/**
	 * Gets the current connection.
	 * @return the current connection.
	 */
	public Connection getConnection() {
		return connection;
	}
	
	@Override
	public boolean addCustomer(Customer customer) {
		String fname = customer.getFirstName();
		String lname = customer.getLastName();
		String phone = customer.getPhone();
		String email = customer.getEmail();
		String street = customer.getAddress();
		String city = customer.getCity();
		String apt = customer.getApt();
		String state = customer.getState();
		String zip = customer.getZip();
		Date dob = customer.getDob();
		
		PreparedStatement stmt = null;		
		
		String query = ("INSERT INTO `customer` (`cid`, `fname`, `lname`, `phone`, `email`, `street`, `city`, `apt`, `state`, `zip`, `dob`) "
				+ "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
		
		try {
			this.establishConnection();
			stmt = connection.prepareStatement(query);
			
			stmt.setString(1, fname);
			stmt.setString(2, lname);
			stmt.setString(3, phone);
			if(!email.equals("")) { stmt.setString(4, email); } else { stmt.setNull(4, java.sql.Types.NULL); }
			stmt.setString(5, street);
			stmt.setString(6, city);
			if(!apt.equals("")) { stmt.setString(7, apt); } else { stmt.setNull(7, java.sql.Types.NULL); }
			stmt.setString(8, state);
			stmt.setString(9, zip);
			stmt.setDate(10, dob);
			
			System.out.println(stmt.toString());
			
			stmt.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			this.closeConnection();
		}
	}
	
	public boolean editCustomer(Customer customer) {
		int id = customer.getCid();
		String fname = customer.getFirstName();
		String lname = customer.getLastName();
		String phone = customer.getPhone();
		String email = customer.getEmail();
		String street = customer.getAddress();
		String city = customer.getCity();
		String apt = customer.getApt();
		String state = customer.getState();
		String zip = customer.getZip();
		Date dob = customer.getDob();
		
		PreparedStatement stmt = null;		

		String query = ("UPDATE `customer` SET " 
				+ "fname = ?, lname = ?, phone = ?, email = ?, street = ?, city = ?, apt = ?, state = ?, zip = ?, dob = ?"
				+ "WHERE cid = ?");
		
		try {
			this.establishConnection();
			stmt = connection.prepareStatement(query);
			
			stmt.setString(1, fname);
			stmt.setString(2, lname);
			stmt.setString(3, phone);
			if(!email.equals("")) { stmt.setString(4, email); } else { stmt.setNull(4, java.sql.Types.NULL); }
			stmt.setString(5, street);
			stmt.setString(6, city);
			if(!apt.equals("")) { stmt.setString(7, apt); } else { stmt.setNull(7, java.sql.Types.NULL); }
			stmt.setString(8, state);
			stmt.setString(9, zip);
			stmt.setDate(10, dob);
			stmt.setInt(11, id);
			
			System.out.println(stmt.toString());
			
			stmt.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			this.closeConnection();
		}
	}
	
	@Override
	public boolean authenticate(String username, String password, String secret) {
		
		String query = ("SELECT * FROM users WHERE username=? AND password=AES_ENCRYPT(?, ?);");
		try {
			this.establishConnection();

			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.setString (3, secret);
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			return rs.next();
		} catch (SQLException e) {
			return false;
		} finally {
			this.closeConnection();
		}
	}
	
	/**
	 * Gets a string representation of the current user.
	 * @return a string representation of the current user.
	 */
	public String getCurrentUser() {
		String query = ("SELECT fname, lname FROM employee t1 INNER JOIN users t2 ON t1.eID = t2.eID WHERE t2.username=? ;");
		try {
			this.establishConnection();

			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setString(1, LoginHandler.getSessionToken().getUsername());
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			StringBuilder sb = new StringBuilder();
			while(rs.next()) {
				sb.append(rs.getString(1));
				sb.append(" ");
				sb.append(rs.getString(2));
			}
			return sb.toString();
		} catch (SQLException e) {
			return "";
		} finally {
			this.closeConnection();
		}
	}
	
	/**
	 * Asks the server if the current user is an administrator.
	 * @return true if admin, no otherwise.
	 */
	public boolean isUserAdmin(String user) {
		String query = ("SELECT admin FROM users WHERE username=?;");
		try {
			this.establishConnection();

			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setString(1, user);
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			rs.next();
			String s = rs.getString(1);
			if(s.equals("1")) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
		} finally {
			this.closeConnection();
		}
		return false;
	}
	
	public int getEmployeeIDByUsername(String username) {
		int eID = 0;
		String query = ("SELECT eID FROM employee WHERE username=?;");
		try {
			this.establishConnection();

			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setString(1, username);
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			rs.next();
			
			eID = rs.getInt(1);	
			
		} catch (SQLException e) {
			
		} finally {
			this.closeConnection();
			
		}
		return eID;
	}

	@Override
	public boolean addFurniture(Furniture furniture) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addEmployee(Employee employee) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Customer[] fetchAllCustomers() {
		throw new UnsupportedOperationException();
	}
	
	public ArrayList<Customer> fetchCustomersByCriteria(String column, String attribute) {
		
		String query = ("SELECT * FROM customer WHERE " + column + "=?;");
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		this.establishConnection();
		try {
			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setString(1, attribute);
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			while (rs.next()) {
				
				Customer customer = new Customer(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7),
						rs.getString(8),
						rs.getString(9),
						rs.getString(10),
						rs.getDate(11)
						);
				
				customers.add(customer);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.closeConnection();
		return customers;
	}
	
	public ArrayList<Furniture> fetchFurnitureByCriteria(String column, String attribute) {
		
		String query = ("SELECT * FROM furniture WHERE " + column + "=?;");
		
		ArrayList<Furniture> furnitureList = new ArrayList<Furniture>();
		this.establishConnection();
		try {
			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setString(1, attribute);
			stmt.execute();
			ResultSet rs = stmt.getResultSet();
			while (rs.next()) {
				
				Furniture furniture = new Furniture(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getDouble(5)
						);
				
				furnitureList.add(furniture);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.closeConnection();
		return furnitureList;
	}
	
	
	@Override
	public Employee[] fetchAllEmployees() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Furniture[] fetchAllFurniture() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeCustomer(int customerID) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeFurniture(int furnitureID) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeEmployee(int employeeID) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean rentItems(FurnitureCheckOuts checkOut) {
		int cid = checkOut.getCustomer().getCid();
		int eid = checkOut.getOverseeingEmployee().getEmployeeID();
		Date transDate = checkOut.getCheckoutDate();
		Date dueDate = checkOut.getDueDate();
		
		PreparedStatement stmt = null;		
		
		String query = ("INSERT INTO `rental_transaction` (`rentalTransactionID`, `customerID`, `eid`, `rentalDateTime`, `dueDate`) "
				+ "VALUES (NULL, ?, ?, ?, ?);");
		
		try {
			this.establishConnection();
			stmt = connection.prepareStatement(query);
			
			stmt.setInt(1, cid);
			stmt.setInt(2, eid);
			stmt.setDate(3, transDate);
			stmt.setDate(4, dueDate);
			
			System.out.println(stmt.toString());
			
			stmt.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			this.closeConnection();
		}
	}

	@Override
	public boolean clearAllData() {
		// TODO Auto-generated method stub
		return false;
	}

}
