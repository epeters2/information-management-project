package data;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Customer;

/**
 * Handles fetching customer data from the database
 * @author Justin
 *
 */
public class CustomerQuery extends QueryHandler {

	/**
	 * Edits the specified customer
	 * @param customer the customer to edit
	 * @postcondition the corresponding customer in the database is updated to match customer
	 * @return whether the customer was successfully edited
	 */
	public boolean editCustomer(Customer customer) {
		int id = customer.getCid();
		String fname 		= customer.getFirstName();
		String lname 		= customer.getLastName();
		String phone 		= customer.getPhone();
		String email 		= customer.getEmail();
		String street 		= customer.getAddress();
		String city 		= customer.getCity();
		String apt 			= customer.getApt();
		String state 		= customer.getState();
		String zip 			= customer.getZip();
		Date dob 			= customer.getDob();

		PreparedStatement stmt;

		String query = ("UPDATE `customer` SET "
				+ "fname = ?, lname = ?, phone = ?, email = ?, street = ?, city = ?, apt = ?, state = ?, zip = ?, dob = ?"
				+ " WHERE cid = ?");

		try {
			establishConnection();
			getConnection();
			stmt = connection.prepareStatement(query);

			stmt.setString(1, fname);
			stmt.setString(2, lname);
			stmt.setString(3, phone);
			if (!email.equals("")) {
				stmt.setString(4, email);
			} else {
				stmt.setNull(4, java.sql.Types.NULL);
			}
			stmt.setString(5, street);
			stmt.setString(6, city);
			if (!apt.equals("")) {
				stmt.setString(7, apt);
			} else {
				stmt.setNull(7, java.sql.Types.NULL);
			}
			stmt.setString(8, state);
			stmt.setString(9, zip);
			stmt.setDate(10, dob);
			stmt.setInt(11, id);
			stmt.execute();
			return true;
		} catch (SQLException e) {
			return false;
		} finally {
			closeConnection();
		}
	}

	/**
	 * Addes the specified customer to the database
	 * @param customer the customer to add
	 * @postcondition the database is updated to include the specified customer
	 * @return whether the customer was successfully added
	 */
	public boolean addCustomer(Customer customer) {
		String fname 		= customer.getFirstName();
		String lname 		= customer.getLastName();
		String phone 		= customer.getPhone();
		String email		= customer.getEmail();
		String street 		= customer.getAddress();
		String city 		= customer.getCity();
		String apt 			= customer.getApt();
		String state 		= customer.getState();
		String zip 			= customer.getZip();
		Date dob 			= customer.getDob();

		PreparedStatement stmt;

		String query = ("INSERT INTO `customer` (`cid`, `fname`, `lname`, `phone`, "
				+ "`email`, `street`, `city`, `apt`, `state`, `zip`, `dob`) "
				+ "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

		try {

			establishConnection();
			getConnection();
			stmt = connection.prepareStatement(query);

			stmt.setString(1, fname);
			stmt.setString(2, lname);
			stmt.setString(3, phone);
			if (!email.equals("")) {
				stmt.setString(4, email);
			} else {
				stmt.setNull(4, java.sql.Types.NULL);
			}
			stmt.setString(5, street);
			stmt.setString(6, city);
			if (!apt.equals("")) {
				stmt.setString(7, apt);
			} else {
				stmt.setNull(7, java.sql.Types.NULL);
			}
			stmt.setString(8, state);
			stmt.setString(9, zip);
			stmt.setDate(10, dob);

			stmt.execute();
			return true;
		} catch (SQLException e) {
			return false;
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * Executs a search with the specified parameters
	 * @param args the parameters for the customer search
	 * @return the results of the search
	 * @throws SQLException
	 */
	public ResultSet executeCustomerSearch(String args[]) throws SQLException {
		return this.executeTableSearch(args, QueryBuilder.CUSTOMER_TABLE);
	}
}
