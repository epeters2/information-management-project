package model;

import java.sql.SQLException;

import controller.ApplicationRegistry;

public class Furniture extends Relation {
	
	private int fid;
	private int iid;
	private String description;
	private double dailyRate;
	private double lateFee;
	private String type;
	private String style;
	private int onHand;
	
	public Furniture(int _fid, int _iid, String _type, String _style, String _descr, double _price, double _lateFee) {
		this.setFid(_fid);
		this.setIid(_iid);
		this.description = _descr;
		this.setPrice(_price);
		this.setLateFee(_lateFee);
		this.setType(_type);
		this.setStyle(_style);
		try {
			onHand = ApplicationRegistry.get().getDatasource()
			.getFurnitureQuery().getInventoryItem(this).getOnHand();
		} catch (SQLException e) {
			onHand = 0;
		}
	}
 
	
	public String getDescription() {
		return description;
	}

	/**
	 * @return the fID
	 */
	public int getFid() {
		return fid;
	}

	/**
	 * @param fID the fID to set
	 */
	public void setFid(int fID) {
		this.fid = fID;
	}

	/**
	 * @return the price
	 */
	public double getDailyRate() {
		return dailyRate;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.dailyRate = price;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}
	
	@Override
	public String toString() {
		return getDescription();
	}

	/**
	 * @return the lateFee
	 */
	public double getLateFee() {
		return lateFee;
	}

	/**
	 * @param lateFee the lateFee to set
	 */
	public void setLateFee(double lateFee) {
		this.lateFee = lateFee;
	}

	/**
	 * @return the iid
	 */
	public int getIid() {
		return iid;
	}

	/**
	 * @param iid the iid to set
	 */
	public void setIid(int iid) {
		this.iid = iid;
	}
	
	
	public int getOnHand() throws SQLException {
		return onHand;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fid;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Furniture))
			return false;
		Furniture other = (Furniture) obj;
		if (fid != other.fid)
			return false;
		return true;
	}
}
