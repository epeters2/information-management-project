package controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Controlls the root layout.
 * @author Justin
 *
 */
public class RootController {
	
	@FXML private Label label;
	
	private BorderPane rootLayout;
	private Stage primaryStage;
	
	/**
	 * Creates a new RootController
	 * @param rootLayout the layout of the root
	 * @param primaryStage the primary stage of the root.
	 *@postcondition this.rootLayout == rootLayout, this.primaryStage == primaryStage
	 */
	public RootController(BorderPane rootLayout, Stage primaryStage) {
		this.rootLayout = rootLayout;
		this.primaryStage = primaryStage;
	}
	
	/**
	 * Initializes and returns the root layout 
	 * @return the root layout
	 * @postcondition the root layout is loaded
	 */
	public BorderPane initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("../view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.centerOnScreen();
            primaryStage.show();            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rootLayout;
    }
	
	/**
     * Gets the root layout currently displayed in the main window
     * @return rootLayout
     */
    public BorderPane getRootLayout() {
    	return rootLayout;
    }
    
	/**
	 * Sets the root layout.
	 * @param root_layout the desired root layout.
	 */
	public void setRootLayout(BorderPane root_layout) {
		rootLayout = root_layout;
	}

}
