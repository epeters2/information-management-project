package controller;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import data.QueryHandler;
import model.Customer;
import model.Employee;
import model.FurnitureCheckOuts;
import model.LoginHandler;
import model.ShoppingCart;

/**
 * Handles logic for checkout
 * @author Justin
 *
 */
public class CheckoutController {
	private ShoppingCart cart = ApplicationRegistry.get().getShoppingCart();
	
	/**
	 * Gets the customer
	 * @return the customer
	 */
	public Customer getCustomer() {
		return cart.getCustomer();
	}

	/**
	 * Removes the specified item from the cart.
	 * @param item the item to be removed
	 * @postcondition this.cart -= item.
	 */
	public void removeCartItem(FurnitureCheckOuts item) {
		cart.removeItemFromCart(item);
	}
	
	/**
	 * Gets all items from the cart.
	 * @return all items from the cart.
	 */
	public ArrayList<FurnitureCheckOuts> getAllFromCart() {
		return cart.getCart();
	}

	/**
	 * Checks out all items in cart
	 * @return whether checkout was successful or not
	 * @throws SQLException if there is an issue with the SQL
	 * @postcondition the database is updated to reflect the items rented,  
	 */
	public boolean checkout() throws SQLException {
			String uname = LoginHandler.getSessionToken().getUsername();
			Customer customer = cart.getCustomer();
			
			int eid = this.getDataSource().getSessionQuery()
					.getEmployeeIDByUsername(uname);
			Employee employee = new Employee(eid, uname);
			
			ArrayList<FurnitureCheckOuts> checkouts = ApplicationRegistry.get()
					.getShoppingCart().getCart();
			if(!checkouts.isEmpty()) {
				for(FurnitureCheckOuts i : checkouts) {
					i.setCustomer(customer);
					i.setOverseeingEmployee(employee);
				}
				return this.getDataSource().getTransactionQuery().rentItems(checkouts);
			} else {
				return false;
			}
	}
	
	private QueryHandler getDataSource() {
		return ApplicationRegistry.get().getDatasource();
	}

	/**
	 * Gets the total cost of all items in cart.
	 * @return the total cost of all items in cart
	 */
	public  double getCartTotal() {
		return cart.getTotalDailyPrice();
	}

	/**
	 * Empties the cart of all items.
	 * @postcondition this.cart == an empty array list
	 */
	public void clearCart() {
		 this.getAllFromCart().clear();
	}

	/**
	 * Updates the dueDate
	 * @param date the new dueDate
	 * @postcondition the due date of each item in the cart == date
	 */
	public void updateDueDate(Date date) {
		for(FurnitureCheckOuts checkouts : cart.getCart()) {
			checkouts.setDueDate(date);
		}
	}
}
