package model;

import java.util.ArrayList;

public class ShoppingCart {
	
	private ArrayList<FurnitureCheckOuts> cart;
	private Customer customer;
	private Employee overseeingEmployee;
	
	public ShoppingCart() {
		setCart(new ArrayList<FurnitureCheckOuts>());
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the cart
	 */
	
	public ArrayList<FurnitureCheckOuts> getCart() {
		return cart;
	}
	/**
	 * @param cart the cart to set
	 */
	public void setCart(ArrayList<FurnitureCheckOuts> cart) {
		this.cart = cart;
	}
	
	public void addItemToCart(FurnitureCheckOuts item) {
		if(!getCart().contains(item)) {
			getCart().add(item);
		}
	}
	
	public void removeItemFromCart(FurnitureCheckOuts item) {
		getCart().remove(item);
	}
	
	/**
	 * Totals up the price of all items in the cart.
	 * @return the total
	 */
	public double getTotalDailyPrice() {
		double total = 0;
		for (FurnitureCheckOuts item : cart) {
			total += (item.getDailyRate() * item.getQuantity());
		}
		return total;
	}
	
	public boolean contains(Furniture furniture) {
		for(FurnitureCheckOuts items : getCart()) {
			Furniture f = items.getFurnitureItem();
			if(f.equals(furniture)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return the overseeingEmployee
	 */
	public Employee getOverseeingEmployee() {
		return overseeingEmployee;
	}

	/**
	 * @param overseeingEmployee the overseeingEmployee to set
	 */
	public void setOverseeingEmployee(Employee overseeingEmployee) {
		this.overseeingEmployee = overseeingEmployee;
	}

}
