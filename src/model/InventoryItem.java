package model;

public class InventoryItem {
	
	private int onHand;
	private int id;
	
	public InventoryItem(int onHand, int id) {
		this.setOnHand(onHand);
		this.setId(id);
	}

	/**
	 * @return the onHand
	 */
	public int getOnHand() {
		return onHand;
	}

	/**
	 * @param onHand the onHand to set
	 */
	public void setOnHand(int onHand) {
		this.onHand = onHand;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}
