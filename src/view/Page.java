package view;

import java.io.IOException;
import java.util.Optional;

import controller.ApplicationRegistry;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Page {
	private Pane currentPane;
	/**
	 * Gets the stage from the specific event
	 * 
	 * @precondition none 
	 * @param event the event fired
	 * @return the desired stage
	 */
	public Stage getStage(MouseEvent event) {
		return (Stage) ((Node) event.getSource()).getScene().getWindow();
	}
    
    /**
     * Handles the click event for when the user 
     * selects a button that will navigate to a new page 
     * 
     * @precondition none
     * @postcondition the desired page is navigated to 
     * @param event the event
     * @param page the new page being navigated to 
     * @throws IOException an exception
     */ 
    public void moveToDifferentPage(String page) throws IOException {
    	GUIManager gm = ApplicationRegistry.get().getGuiManager();
	    Stage currentStage = gm.getApplicationStage();
	    BorderPane rootLayout = (BorderPane) gm.getRootLayout();
	    Pane nextPage = gm.loadPane(page);
	    currentPane = nextPage;
		rootLayout.setCenter(nextPage);
		gm.reloadInfo();
		rootLayout.setBottom(gm.getConnectionView());
		
		currentStage.getScene().setRoot(rootLayout);
    }
    
    public void showErrorWindow(String message, String errorType) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(errorType);
		alert.setContentText(message);
		alert.showAndWait();
	}
    
    public Optional<ButtonType> showAlertWindow(String message, String alertType) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(alertType);
		alert.setHeaderText(null);
		alert.setContentText(message);
		return alert.showAndWait();
    }
    
    public void showCriticalErrorWindow() {
    	showErrorWindow("We're sorry, but an unexpected error occured."
				+ " The application must close.", "Critical Application Error");
    	System.exit(1);
    }
    
    public void highlightTextField(TextField textField) {
    	textField.borderProperty().set(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID,
    			CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    }
    
    public void highlightLabel(Label label, Color color) {
    	label.borderProperty().set(new Border(new BorderStroke(color, BorderStrokeStyle.SOLID,
    			CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    }
    
    public void resetTextField(TextField textfield, Border border) {
    	textfield.borderProperty().set(border);
    }
    
    public void resetLabel(Label label, Border border) {
    	label.borderProperty().set(border);
    }

	/**
	 * @return the currentPane
	 */
	public Pane getCurrentPane() {
		return currentPane;
	}
}
